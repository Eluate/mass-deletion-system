﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Mass_Deleter
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            folderBrowserDialog1.ShowDialog();
            if (folderBrowserDialog1.SelectedPath != null)
                textBox1.Text = folderBrowserDialog1.SelectedPath;

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string selectedOption = comboBox1.Text;

            if (selectedOption == "By File Extension")
            {
                textBox2.Text = "Type File Extension Here";
                textBox2.ReadOnly = false;
            }

            if (selectedOption == "By File Name")
            {
                textBox2.Text = "Type File Name Here";
                textBox2.ReadOnly = false;
            }

            if (selectedOption == "By Partial File Name")
            {
                textBox2.Text = "Type Part of File Name Here";
                textBox2.ReadOnly = false;
            }

            if (selectedOption == "All Files")
            {
                textBox2.Text = "Click Delete";
                textBox2.ReadOnly = true;
            }
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            string path = textBox1.Text;
            string selectedOption = comboBox1.Text;

            if (selectedOption == "By File Extension")
            {
                string[] files = Directory.GetFiles(path, "*." + textBox2.Text, SearchOption.AllDirectories);

                foreach (string file in files)
                {
                    File.Delete(file);
                }
            }

            if (selectedOption == "By File Name")
            {
                string[] files1 = Directory.GetFiles(path, textBox2.Text, SearchOption.AllDirectories);
                string[] files2 = Directory.GetFiles(path, textBox2.Text + ".*", SearchOption.AllDirectories);

                foreach (string file in files1)
                {
                    File.Delete(file);
                    ProgramStatus.Text = "Deleting" + file;
                }

                foreach (string file in files2)
                {
                    File.Delete(file);
                    ProgramStatus.Text = "Deleting" + file;
                }
            }


            if (selectedOption == "By Partial File Name") 
            {
                string[] files = Directory.GetFiles(path, "*" + textBox2.Text + "*" + ".*", SearchOption.AllDirectories);

                foreach (string file in files)
                {
                    File.Delete(file);
                    ProgramStatus.Text = "Deleting" + file;
                }
            }


            if (selectedOption == "All Files")
            {
                string[] files = Directory.GetFiles(path, "*.*", SearchOption.AllDirectories);

                foreach (string file in files)
                {
                    File.Delete(file);
                    ProgramStatus.Text = "Deleting" + file;
                }

            }
            
                ProgramStatus.Text = "Deletion Complete";

        }

        private void toolStripLabel1_Click(object sender, EventArgs e)
        {

        }

        private void toolStripLabel1_Click_1(object sender, EventArgs e)
        {

        }
    }
}
